use std::fs::File;
use std::io::{self, Read, Write};
use std::mem::{transmute, size_of};
use std::path::Path;
use std::slice::from_raw_parts;

use crate::colors::Colour;

fn in_place(input: &Vec<u16>) -> Vec<u8> {
    let mut out = Vec::new();

    for v in input {
        let buf: [u8; size_of::<u16>() / size_of::<u8>()] = unsafe {
            transmute(*v)
        };

        out.extend_from_slice(&buf);
    }

    out
}

fn read<P: AsRef<Path>>(path: P) -> io::Result<(u32, u32, Vec<u16>, Vec<u16>)> {
    let mut f = File::open(path)?;
    let mut buffer = [0u8; size_of::<u32>() / size_of::<u8>()];

    f.read(&mut buffer)?;
    let w = unsafe {
        transmute(buffer)
    };

    f.read(&mut buffer)?;
    let h = unsafe {
        transmute(buffer)
    };

    let mut gly_buffer = vec![0u8; (w * h) as usize * size_of::<u16>() / size_of::<u8>()];
    f.read(&mut gly_buffer[..])?;
    let gly = unsafe {
        from_raw_parts(
            gly_buffer[..].as_ptr() as *const u16,
            gly_buffer.len() / (size_of::<u16>() / size_of::<u8>())
        )
    };

    let mut col_buffer = vec![0u8; (w * h) as usize];
    f.read(&mut col_buffer)?;
    let col = unsafe {
        from_raw_parts(
            col_buffer[..].as_ptr() as *const u16,
            col_buffer.len() / (size_of::<u16>() / size_of::<u8>())
        )
    };

    Ok((w, h, gly.to_vec(), col.to_vec()))
}

fn write<P: AsRef<Path>>(path: P, w: u32, h: u32, gly: &Vec<u16>, col: &Vec<u16>) -> io::Result<()> {
    let mut f = File::create(path)?;

    {
        let buf: [u8; size_of::<u32>() / size_of::<u8>()] = unsafe {
            transmute(w)
        };
        f.write(&buf)?;
    }

    {
        let buf: [u8; size_of::<u32>() / size_of::<u8>()] = unsafe {
            transmute(h)
        };
        f.write(&buf)?;
    }

    {
        f.write(&in_place(&gly))?;
    }

    {
        f.write(&in_place(&col))?;
    }

    Ok(())
}

pub struct OlcSprite {
    pub(crate) width: u32,
    pub(crate) height: u32,

    glyphs: Vec<u16>,
    colours: Vec<u16>,
}

impl Default for OlcSprite {
    fn default() -> Self {
        OlcSprite {
            width: 0,
            height: 0,

            glyphs: Vec::new(),
            colours: Vec::new(),
        }
    }
}

impl OlcSprite {
    pub fn new(w: u32, h: u32) -> Self {
        OlcSprite {
            width: w,
            height: h,

            glyphs: vec![' ' as u16; (w*h) as usize],
            colours: vec![Colour::BgBlack as u16; (w*h) as usize],
        }
    }

    pub fn glyph(&self, x: u32, y: u32) -> u16 {
        if x >= self.width || y >= self.height {
            return ' ' as u16;
        }

        self.glyphs[(y * self.width + x) as usize]
    }

    pub fn colour(&self, x: u32, y: u32) -> u16 {
        if x >= self.width || y >= self.height {
            return Colour::BgBlack as u16;
        }

        self.colours[(y * self.width + x) as usize]
    }

    pub fn glyph_mut<'a>(&'a self, x: u32, y: u32) -> &'a u16 {
        &self.glyphs[(y * self.width + x) as usize]
    }

    pub fn colour_mut<'a>(&'a mut self, x: u32, y: u32) -> &'a u16 {
        &self.colours[(y * self.width + x) as usize]
    }

    pub fn sample_glyph(&self, x: f32, y: f32) -> u16 {
        let sx = (x * self.width as f32) as i32;
        let sy = (y * self.height as f32) as i32;

        if sx < 0 || sx >= (self.width as i32) || sy < 0 || sy >= (self.height as i32) {
            return ' ' as u16;
        }

        self.glyphs[(sy * (self.width as i32) + sx) as usize]
    }

    pub fn sample_colour(&self, x: f32, y: f32) -> u16 {
        let sx = (x * self.width as f32) as i32;
        let sy = (y * self.height as f32) as i32;

        if sx < 0 || sx >= (self.width as i32) || sy < 0 || sy >= (self.height as i32) {
            return Colour::BgBlack as u16;
        }

        self.colours[(sy * (self.width as i32) + sx) as usize]
    }

    pub fn load<P>(&mut self, path: P) -> io::Result<()> where P: AsRef<Path> {
        let out = read(path)?;

        self.width   = out.0;
        self.height  = out.1;
        self.glyphs  = out.2;
        self.colours = out.3;

        Ok(())
    }

    pub fn save<P>(&self, path: P) -> io::Result<()> where P: AsRef<Path> {
        write(path, self.width, self.height, &self.glyphs, &self.colours)?;

        Ok(())
    }
}

impl<P: AsRef<Path>> From<P> for OlcSprite {
    fn from(path: P) -> Self {
        let out = match read(path) {
            Ok(o) => o,
            Err(_) => { return Self::new(8, 8); },
        };

        Self {
            width   : out.0,
            height  : out.1,
            glyphs  : out.2,
            colours : out.3,
        }
    }
}
