use sdl2::EventPump;
use sdl2::event::{Event, WindowEvent};
pub use sdl2::keyboard::Scancode;
pub use sdl2::mouse::MouseButton;

use std::ops::Index;

#[derive(Clone,Copy,Default,Debug)]
pub struct KeyState {
    pub pressed: bool,
    pub released: bool,
    pub held: bool,
}

pub(crate) struct KeyStateWrap {
    state: [KeyState; 512],
}

impl Default for KeyStateWrap {
    fn default() -> Self {
        KeyStateWrap {
            state: [Default::default(); 512]
        }
    }
}

impl Index<Scancode> for KeyStateWrap {
    type Output = KeyState;
    fn index(&self, n_key_id: Scancode) -> &Self::Output {
        &self.state[ n_key_id as usize ]
    }
}

/// Events structure keep trace of every kind of event happening within the SDL2 window.
pub struct Events {
    key_state_old: [bool; 512],
    key_state_new: [bool; 512],
    mouse_state_old: [bool; 6],
    mouse_state_new: [bool; 6],

    focus: bool,

    keys: KeyStateWrap,
    mouse: [KeyState; 6],

    pub mouse_x: i32,
    pub mouse_y: i32,

    /// Mouse x position in console space:
    pub mouse_pos_x: i32,
    /// Mouse y position in console space:
    pub mouse_pos_y: i32,

    /// Real sdl2 window width:
    pub(crate) screen_width: i32,
    /// Real sdl2 window height:
    pub(crate) screen_height: i32,

    font_width: i32,
    font_height: i32,
}

impl Default for Events {
    fn default() -> Self {
        Self {
            key_state_old: [false; 512],
            key_state_new: [false; 512],

            mouse_state_old: [false; 6],
            mouse_state_new: [false; 6],

            focus: true,

            keys: Default::default(),

            mouse: [Default::default(); 6],

            mouse_x: 0,
            mouse_y: 0,

            mouse_pos_x: 0,
            mouse_pos_y: 0,

            screen_width: 0,
            screen_height: 0,

            font_width: 0,
            font_height: 0,
        }
    }
}

impl Events {
    pub(crate) fn new(sw: i32, sh: i32, fw: i32, fh: i32) -> Self {
        let mut new: Self = Default::default();

        new.screen_width  = sw;
        new.screen_height = sh;

        new.font_width  = fw;
        new.font_height = fh;

        new
    }

    pub(crate) fn update(&mut self, pump: &mut EventPump) -> bool {
        // Finding out which keys where pressed/released or what other kind of supported event
        // happened:
        for event in pump.poll_iter() {
            match event {
                Event::Quit            { .. }                                            => return false,

                Event::KeyDown         { scancode: Some(x), .. }                         => self.key_state_new[x as usize] = true,
                Event::KeyUp           { scancode: Some(x), .. }                         => self.key_state_new[x as usize] = false,

                Event::MouseButtonDown { mouse_btn: MouseButton::Left, .. }              => self.mouse_state_new[0] = true,
                Event::MouseButtonDown { mouse_btn: MouseButton::Right, .. }             => self.mouse_state_new[1] = true,
                Event::MouseButtonDown { mouse_btn: MouseButton::Middle, .. }            => self.mouse_state_new[2] = true,
                Event::MouseButtonDown { mouse_btn: MouseButton::X1, .. }                => self.mouse_state_new[3] = true,
                Event::MouseButtonDown { mouse_btn: MouseButton::X2, .. }                => self.mouse_state_new[4] = true,
                Event::MouseButtonUp   { mouse_btn: MouseButton::Left, .. }              => self.mouse_state_new[0] = false,
                Event::MouseButtonUp   { mouse_btn: MouseButton::Right, .. }             => self.mouse_state_new[1] = false,
                Event::MouseButtonUp   { mouse_btn: MouseButton::Middle, .. }            => self.mouse_state_new[2] = false,
                Event::MouseButtonUp   { mouse_btn: MouseButton::X1, .. }                => self.mouse_state_new[3] = false,
                Event::MouseButtonUp   { mouse_btn: MouseButton::X2, .. }                => self.mouse_state_new[4] = false,
                Event::MouseMotion     { x, y, .. }                                      => { self.mouse_x = x; self.mouse_y = y; },

                Event::Window          { win_event: WindowEvent::FocusLost, .. }         => self.focus = false,
                Event::Window          { win_event: WindowEvent::FocusGained, .. }       => self.focus = true,

                Event::Window          { win_event: WindowEvent::Resized(x, y), .. }     => { self.screen_width = x; self.screen_height = y;},
                Event::Window          { win_event: WindowEvent::SizeChanged(x, y), .. } => { self.screen_width = x; self.screen_height = y;},

                _ => {},
            }
        }

        self.mouse_pos_x = self.mouse_x / self.font_width;
        self.mouse_pos_y = self.mouse_y / self.font_height;

        // Updating the key status, if it has changed, for the user to use:
        for i in 0..self.keys.state.len() {
            self.keys.state[i].pressed  = false;
            self.keys.state[i].released = false;

            if self.key_state_new[i] != self.key_state_old[i] {
                if self.key_state_new[i] {
                    self.keys.state[i].pressed = ! self.keys.state[i].held;
                    self.keys.state[i].held = true;
                } else {
                    self.keys.state[i].released = true;
                    self.keys.state[i].held = false;
                }
            }

            self.key_state_old[i] = self.key_state_new[i];
        }

        // Updating the mouse status, if it has changed, for the user to use:
        for i in 0..self.mouse.len() {
            self.mouse[i].pressed  = false;
            self.mouse[i].released = false;

            if self.mouse_state_new[i] != self.mouse_state_old[i] {
                if self.mouse_state_new[i] {
                    self.mouse[i].pressed = true; // ! self.mouse[i].held;
                    self.mouse[i].held = true;
                } else {
                    self.mouse[i].released = true;
                    self.mouse[i].held = false;
                }
            }

            self.mouse_state_old[i] = self.mouse_state_new[i];
        }

        true
    }
}

impl Index<MouseButton> for Events {
    type Output = KeyState;

    fn index(&self, btn: MouseButton) -> &Self::Output {
        &self.mouse[btn as usize]
    }
}

impl Index<Scancode> for Events {
    type Output = KeyState;

    fn index(&self, key: Scancode) -> &Self::Output {
        &self.keys[key]
    }
}
