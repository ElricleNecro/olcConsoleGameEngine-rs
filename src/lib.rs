extern crate log;

extern crate sdl2;

use std::time::{Duration, SystemTime};

use log::{info, debug, error, warn};

use sdl2::Sdl;
use sdl2::pixels::{Color, PixelFormatEnum::RGBA8888};
use sdl2::rect::Rect;
use sdl2::render::{/*Texture, TextureAccess, TextureCreator,*/ WindowCanvas};
use sdl2::surface::Surface;
// use sdl2::video::WindowContext;
// use sdl2::video::Window;

mod colors;
pub use crate::colors::{Colour, PIXEL_SOLID, PIXEL_THREEQUARTERS, PIXEL_HALF, PIXEL_QUARTER};
use crate::colors::COLOUR_LOOKUP;

mod sprite;
pub use crate::sprite::OlcSprite;

pub mod events;
pub use crate::events::Events;

mod buffer;
pub use crate::buffer::ScreenBuffer;

// const KEY_OFFSET: u8 = 256;

pub trait EngineInterface {
    fn on_user_create(&mut self, olc: &mut ScreenBuffer)-> bool;
    fn on_user_update(&mut self, olc: &mut ScreenBuffer, events: &Events, elapsed_time: f32) -> bool;

    fn on_user_destroy(&mut self) -> bool {
        true
    }
}

#[derive(Clone,Copy,Default,Debug)]
pub struct CharInfo {
    pub glyph: u16,
    pub colour: Colour,
}

pub struct OlcConsoleGameEngine {
    font_width: i32,
    font_height: i32,

    buffers: ScreenBuffer,

    app_name: String,

    events: Events,
    framerate: u64,

    // SDL2 data:
    context: Sdl,
    canvas: WindowCanvas,

    state: Box<EngineInterface>,
}

pub struct OlcConsoleGameEngineBuilder {
    screen_width: i32,
    screen_height: i32,

    font_width: i32,
    font_height: i32,

    app_name: String,

    framerate: u64,
}

impl OlcConsoleGameEngineBuilder {
    pub fn new() -> Self {
        OlcConsoleGameEngineBuilder {
            screen_width: 0,
            screen_height: 0,

            font_width: 8,
            font_height: 8,

            app_name: "Default".to_string(),

            framerate: (1000. / 60.) as u64
        }
    }

    pub fn screen_width(&mut self, w: i32) -> &mut Self {
        self.screen_width = w;
        self
    }

    pub fn screen_height(&mut self, h: i32) -> &mut Self {
        self.screen_height = h;
        self
    }

    pub fn font_width(&mut self, w: i32) -> &mut Self {
        self.font_width = w;
        self
    }

    pub fn font_height(&mut self, h: i32) -> &mut Self {
        self.font_height = h;
        self
    }

    pub fn title(&mut self, t: String) -> &mut Self {
        self.app_name = t;
        self
    }

    pub fn fps(&mut self, t: f64) -> &mut Self {
        self.framerate = (1000. / t) as u64;
        self
    }

    pub fn build(&self, state: Box<EngineInterface>) -> Result<OlcConsoleGameEngine, String> {
        let size : usize = (self.screen_width as usize) * (self.screen_height as usize);
        let context = sdl2::init()?;
        let video   = context.video()?;
        let window  = match video.window(
            &self.app_name,
            (self.screen_width * self.font_width) as u32,
            (self.screen_height * self.font_height) as u32)
            .position_centered()
            .allow_highdpi()
            // .resizable()
            .build() {
                Ok(w) => w,
                Err(e) => { return Err(format!("Error while creating the window: {}.", e)); }
            };
        let canvas = match window.into_canvas()
            .accelerated()
            .target_texture()
            .present_vsync()
            .build() {
                Ok(w) => w,
                Err(e) => { return Err(format!("Error while creating the window: {}.", e)); }
            };

        Ok(OlcConsoleGameEngine {
            font_width: self.font_width,
            font_height: self.font_height,

            buffers: ScreenBuffer {
                screen_width: self.screen_width as u32,
                screen_height: self.screen_height as u32,

                buffer: [vec![Default::default(); size], vec![Default::default(); size]],
                cur_buffer: 0,
            },

            app_name: self.app_name.clone(),

            events: Events::new(
                self.screen_width  * self.font_width,
                self.screen_height * self.font_height,

                self.font_width,
                self.font_height
            ),
            framerate: self.framerate,

            context: context,
            canvas: canvas,

            state: state,
        })
    }
}

impl OlcConsoleGameEngine {
    pub fn start(&mut self) -> Result<(), String> {
        let tex_creator = self.canvas.texture_creator();
        let mut surf = Surface::load_bmp("olcfont_consolas.bmp")?;
        surf.set_color_key(true, Color::RGB(255, 0, 255))?;

        let mut font = match tex_creator.create_texture_from_surface(surf) {
            Ok(t) => t,
            Err(e) => return Err(format!("Error while loading the fonts: '{}'", e)),
        };

        let mut screen = match tex_creator.create_texture_target(RGBA8888, self.events.screen_width as u32, self.events.screen_height as u32) {
            Ok(t) => t,
            Err(e) => return Err(format!("Error while loading the fonts: '{}'", e)),
        };

        let mut pump = self.context.event_pump()?;

        let mut elapsed_time: f32 = 1.;

        if ! self.state.on_user_create(&mut self.buffers) {
            return Err("on_user_create failed.".to_owned());
        }

        self.canvas.clear();

        let mut cc = &mut self.canvas;
        let fps = Duration::from_millis(self.framerate);
        'game: loop {
            'inner: loop {
                let time = SystemTime::now();
                let mut updated = false;

                if ! self.events.update(&mut pump) {
                    break 'game;
                }

                self.buffers.screen_width  = (self.events.screen_width / self.font_width) as u32;
                self.buffers.screen_height = (self.events.screen_height / self.font_height) as u32;

                if ! self.state.on_user_update(&mut self.buffers, &self.events, elapsed_time) {
                    break 'inner;
                }

                match cc.window_mut().set_title(
                    &format!(
                        "{} FPS: {} ({})",
                        self.app_name,
                        1f64 / (elapsed_time as f64),
                        elapsed_time
                    )
                ) {
                    Ok(_) => {},
                    Err(e) => {
                        warn!("Error while getting elapsed time: {}", e);
                    }
                };
                info!("FPS: {}", 1f64 / elapsed_time as f64);

                let old_buf = &self.buffers.buffer[(self.buffers.cur_buffer + 1) % 2];
                let new_buf = &self.buffers.buffer[self.buffers.cur_buffer];

                // debug!("------------------------------------------------------------------------");
                let buf = &self.buffers;
                let (fw, fh) = (self.font_width, self.font_height);
                cc.with_texture_canvas(&mut screen, |canvas| {
                    // canvas.set_draw_color(Color::RGB(0, 0, 0));
                    // canvas.clear();

                    for x in 0..buf.screen_width {
                        for y in 0..buf.screen_height {
                            let idx = (y * buf.screen_width + x) as usize;

                            if new_buf[idx].colour != old_buf[idx].colour ||
                               new_buf[idx].glyph  != old_buf[idx].glyph {
                                debug!("Plotting!");
                                updated = true;
                                // Cell coordinate:
                                let cx   = new_buf[idx].glyph % 64;
                                let cy   = new_buf[idx].glyph / 64;

                                // Draw foreground:
                                let cidx = ((new_buf[idx].colour as u16) & 0x00F0) >> 4;
                                let bg_col = COLOUR_LOOKUP[(((new_buf[idx].colour as u16) & 0x00F0) >> 4) as usize];
                                let src_bg = Rect::new((cidx * 16).into(), 0, 16, 16);
                                let dst    = Rect::new(x as i32 * fw, y as i32 * fh, fw as u32, fh as u32);

                                font.set_color_mod(bg_col.r, bg_col.g, bg_col.b);
                                font.set_alpha_mod(bg_col.a);
                                match canvas.copy(&font, src_bg, dst) {
                                    Ok(_) => {},
                                    Err(e) => {error!("Error while copying to screen: {}.", e)},
                                };

                                // Set font colour:
                                let fg_col = COLOUR_LOOKUP[((new_buf[idx].colour as u16) & 0x000F) as usize];
                                font.set_color_mod(fg_col.r, fg_col.g, fg_col.b);
                                font.set_alpha_mod(fg_col.a);
                                let src_fg = Rect::new((cx * 16).into(), (cy * 16).into(), 16, 16);
                                match canvas.copy(&font, src_fg, dst) {
                                    Ok(_) => {},
                                    Err(e) => {error!("Error while copying to screen: {}.", e)},
                                };
                            }
                        }
                    }
                });
                // debug!("------------------------------------------------------------------------");

                cc.copy(&screen, None, None)?;
                if updated {
                    cc.present();
                }
                self.buffers.cur_buffer = (self.buffers.cur_buffer + 1) % 2;

                let elapsed = match time.elapsed() {
                    Ok(v) => v,
                    Err(e) => {
                        warn!("Error while getting elapsed time: {}", e);
                        continue;
                    }
                };

                if elapsed < fps {
                    std::thread::sleep(fps - elapsed);
                }

                elapsed_time = match time.elapsed() {
                    Ok(v) => (v.as_secs() * 1_000_000_000 + v.subsec_nanos() as u64) as f32 / 1e9,
                    Err(e) => {
                        warn!("Error while getting elapsed time: {}", e);
                        continue;
                    }
                };
            }

            if self.state.on_user_destroy() {
                break 'game;
            }
        }

        Ok(())
    }
}
