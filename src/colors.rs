use sdl2::pixels::Color;

#[derive(Clone,Copy,Debug,PartialEq)]
pub enum Colour {
    FgBlack = 0x0000,
    FgDarkBlue = 0x0001,
    FgDarkGreen = 0x0002,
    FgDarkCyan = 0x0003,
    FgDarkRed = 0x0004,
    FgDarkMagenta = 0x0005,
    FgDarkYellow = 0x0006,
    FgGrey = 0x0007,
    FgDarkGrey = 0x0008,
    FgBlue = 0x0009,
    FgGreen = 0x000A,
    FgCyan = 0x000B,
    FgRed = 0x000C,
    FgMagenta = 0x000D,
    FgYellow = 0x000E,
    FgWhite = 0x000F,
    BgDarkBlue = 0x0010,
    BgDarkGreen = 0x0020,
    BgDarkCyan = 0x0030,
    BgDarkRed = 0x0040,
    BgDarkMagenta = 0x0050,
    BgDarkYellow = 0x0060,
    BgGrey = 0x0070,
    BgDarkGrey = 0x0080,
    BgBlue = 0x0090,
    BgGreen = 0x00A0,
    BgCyan = 0x00B0,
    BgRed = 0x00C0,
    BgMagenta = 0x00D0,
    BgYellow = 0x00E0,
    BgWhite = 0x00F0,
    BgBlack = 0x0100,
    // FillBlack = 0x0100,
}

impl Default for Colour {
    fn default() -> Self {
        Colour::FgBlack
    }
}

impl From<u16> for Colour {
    fn from(v: u16) -> Self {
        match v {
            0x0000 => Colour::FgBlack,
            0x0001 => Colour::FgDarkBlue,
            0x0002 => Colour::FgDarkGreen,
            0x0003 => Colour::FgDarkCyan,
            0x0004 => Colour::FgDarkRed,
            0x0005 => Colour::FgDarkMagenta,
            0x0006 => Colour::FgDarkYellow,
            0x0007 => Colour::FgGrey,
            0x0008 => Colour::FgDarkGrey,
            0x0009 => Colour::FgBlue,
            0x000A => Colour::FgGreen,
            0x000B => Colour::FgCyan,
            0x000C => Colour::FgRed,
            0x000D => Colour::FgMagenta,
            0x000E => Colour::FgYellow,
            0x000F => Colour::FgWhite,
            0x0010 => Colour::BgDarkBlue,
            0x0020 => Colour::BgDarkGreen,
            0x0030 => Colour::BgDarkCyan,
            0x0040 => Colour::BgDarkRed,
            0x0050 => Colour::BgDarkMagenta,
            0x0060 => Colour::BgDarkYellow,
            0x0070 => Colour::BgGrey,
            0x0080 => Colour::BgDarkGrey,
            0x0090 => Colour::BgBlue,
            0x00A0 => Colour::BgGreen,
            0x00B0 => Colour::BgCyan,
            0x00C0 => Colour::BgRed,
            0x00D0 => Colour::BgMagenta,
            0x00E0 => Colour::BgYellow,
            0x00F0 => Colour::BgWhite,
            0x0100 => Colour::BgBlack,
            _      => Default::default(),
        }
    }
}

pub static PIXEL_SOLID: u16 = 0x2588;
pub static PIXEL_THREEQUARTERS: u16 = 0x2593;
pub static PIXEL_HALF: u16 = 0x2592;
pub static PIXEL_QUARTER: u16 = 0x2591;

pub static COLOUR_LOOKUP: [Color; 17] = [
    Color{ r: 0,   g: 0,   b: 0,   a:   0,  }, // 0
    Color{ r: 0,   g: 0,   b: 127, a: 255,  }, // 1
    Color{ r: 0,   g: 127, b: 0,   a: 255,  }, // 2
    Color{ r: 0,   g: 127, b: 127, a: 255,  }, // 3
    Color{ r: 127, g: 0,   b: 0,   a: 255,  }, // 4
    Color{ r: 127, g: 0,   b: 127, a: 255,  }, // 5
    Color{ r: 127, g: 127, b: 0,   a: 255,  }, // 6
    Color{ r: 192, g: 192, b: 192, a: 255,  }, // 7
    Color{ r: 127, g: 127, b: 127, a: 255,  }, // 8
    Color{ r: 0,   g: 0,   b: 255, a: 255,  }, // 9
    Color{ r: 0,   g: 255, b: 0,   a: 255,  }, // A
    Color{ r: 0,   g: 255, b: 255, a: 255,  }, // B
    Color{ r: 255, g: 0,   b: 0,   a: 255,  }, // C
    Color{ r: 255, g: 0,   b: 255, a: 255,  }, // D
    Color{ r: 255, g: 255, b: 0,   a: 255,  }, // E
    Color{ r: 255, g: 255, b: 255, a: 255,  }, // F
    Color{ r: 0,   g: 0,   b: 0,   a: 255,  }, // 16: Extra
];

