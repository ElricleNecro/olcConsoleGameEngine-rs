use std::ops::{Index, IndexMut};

use log::debug;

use crate::colors::Colour;
use crate::sprite::OlcSprite;
use super::CharInfo;

pub struct ScreenBuffer {
    pub(crate) screen_width: u32,
    pub(crate) screen_height: u32,

    pub(crate) buffer: [Vec<CharInfo>; 2],
    pub(crate) cur_buffer: usize,
}

impl ScreenBuffer {
    fn clip(&self, x: u32, y: u32) -> (u32, u32) {
        debug!("Clipping: ({}, {})", x, y);

        let x_o = if x >= self.screen_width {
            self.screen_width
        } else {
            x
        };

        let y_o = if y >= self.screen_height {
            self.screen_height
        } else {
            y
        };

        debug!("Clipped: ({}, {})", x_o, y_o);

        (x_o, y_o)
    }

    pub fn fill(&mut self, x1: u32, y1: u32, x2: u32, y2:u32, c: u16, col: Colour) {
        debug!("Filling screen between: ({}, {}) and ({}, {}).", x1, y1, x2, y2);
        debug!("u16 and Color used: {:?}, {:?}.", c, col);
        let (cx1, cy1) = self.clip(x1, y1);
        let (cx2, cy2) = self.clip(x2, y2);

        for ix in cx1..cx2 {
            for iy in cy1..cy2 {
                self[(ix, iy)].glyph  = c as u16;
                self[(ix, iy)].colour = col;
            }
        }
    }

    pub fn draw_string(&mut self, x: u32, y: u32, msg: &str, col: Colour) {
        debug!("Drawing the string: {} at coord ({}, {}) with format {:?}.", msg, x, y, col);
        for (idx, c) in msg.chars().enumerate() {
            debug!("Char({}) -> {} u16.", c, c as u16);
            self[(x + (idx as u32), y)].glyph = c as u16;
            self[(x + (idx as u32), y)].colour = col;
            // self.buffer[self.cur_buffer][(coord.1 * self.screen_width + coord.0 + idx) as usize]
        }
    }

    pub fn draw_string_alpha(&mut self, x: u32, y: u32, msg: &str, col: Colour) {
        debug!("Drawing the string with alpha: {} at coord ({}, {}) with format {:?}.", msg, x, y, col);
        for (idx, c) in msg.chars().enumerate() {
            if c == ' ' {
                continue;
            }

            self[(x + (idx as u32), y)].glyph = c as u16;
            self[(x + (idx as u32), y)].colour = col;
            // self[(x + (idx as u32), y)].colour |= col;
        }
    }

    pub fn draw_line(&mut self, x1: u32, y1: u32, x2: u32, y2: u32, c: u16, col: Colour) {
        debug!("Drawing a line between ({}, {}) and ({}, {}) with format ({:?}, {:?}).", x1, y1, x2, y2, c, col);

        let dx = x2 as i32 - x1 as i32;
        let dy = y2 as i32 - y1 as i32;
        let dx1 = dx.abs();
        let dy1 = dy.abs();
        let end;

        let mut x;
        let mut y;
        let mut px = 2 * dy1 - dx1;
        let mut py = 2 * dx1 - dy1;

        if dy1 <= dx1 {
            if dx >= 0 {
                x = x1;
                y = y1;
                end = x2;
            } else {
                x = x2;
                y = y2;
                end = x1;
            }

            self[(x, y)].glyph = c as u16;
            self[(x, y)].colour = col;

            while x < end {
                x = x + 1;
                if px < 0 {
                    px = px + 2 * dy1;
                } else {
                    if (dx<0 && dy<0) || (dx>0 && dy>0) {
                        y = y + 1;
                    } else {
                        y = y - 1;
                    }
                    px = px + 2 * (dy1 - dx1);
                }
                self[(x, y)].glyph = c as u16;
                self[(x, y)].colour = col;
            }
        } else {
            if dy >= 0 {
                x = x1;
                y = y1;
                end = y2;
            } else {
                x = x2;
                y = y2;
                end = y1;
            }

            self[(x, y)].glyph = c as u16;
            self[(x, y)].colour = col;

            while y < end {
                y = y + 1;
                if py <= 0 {
                    py = py + 2 * dx1;
                } else {
                    if (dx<0 && dy<0) || (dx>0 && dy>0) {
                        x = x + 1;
                    } else {
                        x = x - 1;
                    }
                    py = py + 2 * (dx1 - dy1);
                }
                self[(x, y)].glyph = c as u16;
                self[(x, y)].colour = col;
            }
        }
    }

    pub fn draw_circle(&mut self, xc: u32, yc: u32, r: i32, c: u16, col: Colour) {
        debug!("Drawing a circle at position ({}, {}) with radius {} and format ({:?}, {:?}).", xc, yc, r, c, col);
        let mut x = 0;
        let mut y = r;
        let mut p = 3 - 2 * r;

        if r == 0 {
            return ;
        }

        while y >= x {
            // upper left left
            //Draw(xc - x, yc - y, c, col)
            self[(((xc as i32) - x) as u32, ((yc as i32) - y) as u32)].glyph  = c as u16;
            self[(((xc as i32) - x) as u32, ((yc as i32) - y) as u32)].colour = col;

            // upper upper left
            //Draw(((xc as i32) - y, (yc as i32) - x, c) as u32, (col) as u32)
            self[(((xc as i32) - y) as u32, ((yc as i32) - x) as u32)].glyph  = c as u16;
            self[(((xc as i32) - y) as u32, ((yc as i32) - x) as u32)].colour = col;

            // upper upper right
            //Draw(((xc as i32) + y, (yc as i32) - x, c) as u32, (col) as u32)
            self[(((xc as i32) + y) as u32, ((yc as i32) - x) as u32)].glyph  = c as u16;
            self[(((xc as i32) + y) as u32, ((yc as i32) - x) as u32)].colour = col;

            // upper right right
            //Draw(((xc as i32) + x, (yc as i32) - y, c) as u32, (col) as u32)
            self[(((xc as i32) + x) as u32, ((yc as i32) - y) as u32)].glyph  = c as u16;
            self[(((xc as i32) + x) as u32, ((yc as i32) - y) as u32)].colour = col;

            // lower left left
            //Draw(((xc as i32) - x, (yc as i32) + y, c) as u32, (col) as u32)
            self[(((xc as i32) - x) as u32, ((yc as i32) + y) as u32)].glyph  = c as u16;
            self[(((xc as i32) - x) as u32, ((yc as i32) + y) as u32)].colour = col;

            // lower lower left
            //Draw(((xc as i32) - y, (yc as i32) + x, c) as u32, (col) as u32)
            self[(((xc as i32) - y) as u32, ((yc as i32) + x) as u32)].glyph  = c as u16;
            self[(((xc as i32) - y) as u32, ((yc as i32) + x) as u32)].colour = col;

            // lower lower right
            //Draw(((xc as i32) + y, (yc as i32) + x, c) as u32, (col) as u32)
            self[(((xc as i32) + y) as u32, ((yc as i32) + x) as u32)].glyph  = c as u16;
            self[(((xc as i32) + y) as u32, ((yc as i32) + x) as u32)].colour = col;

            // lower right right
            //Draw(((xc as i32) + x, (yc as i32) + y, c) as u32, (col) as u32)
            self[(((xc as i32) + x) as u32, ((yc as i32) + y) as u32)].glyph  = c as u16;
            self[(((xc as i32) + x) as u32, ((yc as i32) + y) as u32)].colour = col;

            if p < 0 {
                p += 4 * x + 6;
                x += 1;
            } else {
                p += 4 * (x - y) + 10;
                x += 1;
                y -= 1;
            }
        }
    }

    pub fn draw_fill_circle(&mut self, xc: u32, yc: u32, r: i32, c: u16, col: Colour) {
        debug!("Drawing a filled circle at position ({}, {}) with radius {} and format ({:?}, {:?}).", xc, yc, r, c, col);
        let mut x = 0;
        let mut y = r;
        let mut p = 3 - 2 * r;

        let mut drawline = |sx, ex, ny| {
            for i in sx..=ex {
                self[(i, ny)].glyph = c as u16;
                self[(i, ny)].colour = col;
            }
        };

        if r == 0 {
            return ;
        }

        while y >= x {
            drawline(((xc as i32) - x) as u32, ((xc as i32) + x) as u32, ((yc as i32) - y) as u32);
            drawline(((xc as i32) - y) as u32, ((xc as i32) + y) as u32, ((yc as i32) - x) as u32);
            drawline(((xc as i32) - x) as u32, ((xc as i32) + x) as u32, ((yc as i32) + y) as u32);
            drawline(((xc as i32) - y) as u32, ((xc as i32) + y) as u32, ((yc as i32) + x) as u32);

            if p < 0 {
                p += 4 * x + 6;
                x += 1;
            } else {
                p += 4 * (x - y) + 10;
                x += 1;
                y -= 1;
            }
        }
    }

    pub fn draw_sprite(&mut self, x: u32, y: u32, spr: &OlcSprite) {
        for i in 0..spr.width {
            for j in 0..spr.height {
                if spr.glyph(i, j) != (' ' as u16) {
                    self[(x + i, y + j)].glyph = spr.glyph(i, j);
                    self[(x + i, y + j)].colour = Colour::from( spr.colour(i, j) );
                }
            }
        }
    }

    pub fn draw_partial_sprite(
        &mut self,
        x: u32,
        y: u32,
        spr: &OlcSprite,
        ox: u32,
        oy: u32,
        w: u32,
        h: u32
    ) {
        for i in 0..w {
            for j in 0..h {
                if spr.glyph(i + ox, j + oy) != (' ' as u16) {
                    self[(x + i, y + j)].glyph = spr.glyph(i + ox, j + oy);
                    self[(x + i, y + j)].colour = spr.colour(i + ox, j + oy).into();
                }
            }
        }
    }

    pub fn width(&self) -> u32 {
        self.screen_width
    }

    pub fn height(&self) -> u32 {
        self.screen_height
    }

    pub fn draw_triangle(&mut self, x1: u32, y1: u32, x2: u32, y2: u32, x3: u32, y3: u32, c: u16, col: Colour) {
        self.draw_line(x1, y1, x2, y2, c, col);
        self.draw_line(x2, y2, x3, y3, c, col);
        self.draw_line(x3, y3, x1, y1, c, col);
    }

    pub fn draw_fill_triangle(&mut self, _x1: u32, _y1: u32, _x2: u32, _y2: u32, _x3: u32, _y3: u32, _c: u16, _col: Colour) {
        unimplemented!();
    }
}

impl Index<(u32, u32)> for ScreenBuffer {
    type Output = CharInfo;

    fn index(&self, coord: (u32, u32)) -> &Self::Output {
        &self.buffer[self.cur_buffer][(coord.1 * self.screen_width + coord.0) as usize]
    }
}

impl IndexMut<(u32, u32)> for ScreenBuffer {
    fn index_mut(&mut self, coord: (u32, u32)) -> &mut Self::Output {
        &mut self.buffer[self.cur_buffer][(coord.1 * self.screen_width + coord.0) as usize]
    }
}

