#[macro_use]
extern crate log;
extern crate simple_logger;
extern crate rand;
extern crate olc_console_game_engine;

use log::Level;

use olc_console_game_engine::{EngineInterface, OlcConsoleGameEngineBuilder, ScreenBuffer, Events, Colour, PIXEL_SOLID, PIXEL_HALF};
use olc_console_game_engine::events::{MouseButton, Scancode};

use std::f32;
use std::cmp::Ordering;
use std::collections::VecDeque;

#[derive(Default,Clone)]
struct Node {
    x: i32,
    y: i32,

    obstacle: bool,
    visited: bool,

    global_goal: f32,
    local_goal: f32,
}

struct AStar {
    width: i32,
    height: i32,

    nodes: Vec<Node>,

    node_start: usize,
    node_end: usize,

    neighbours: Vec<Vec<usize>>,
    parent: Vec<usize>,
}

impl AStar {
    pub fn new(w: i32, h: i32) -> Self {
        Self {
            width: w,
            height: h,

            nodes: vec![Default::default(); (w * h) as usize],

            node_start: ((h / 2) * w + 1) as usize,
            node_end:   ((h / 2) * w + w - 2) as usize,

            neighbours: vec![Vec::new(); (w * h) as usize],
            parent: vec![usize::max_value(); (w * h) as usize],
        }
    }

    fn distance(a: &Node, b: &Node) -> f32 {
        (((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y)) as f32).sqrt()
    }

    fn heuristic(a: &Node, b: &Node) -> f32 {
        Self::distance(a, b)
    }

    fn solve_astar(&mut self) {
        for x in 0..self.width {
            for y in 0..self.height {
                self.nodes[(y*self.width + x) as usize].visited = false;
                self.nodes[(y*self.width + x) as usize].global_goal = f32::INFINITY;
                self.nodes[(y*self.width + x) as usize].local_goal = f32::INFINITY;
                self.parent[(y*self.width + x) as usize] = usize::max_value();
            }
        }

        let mut node_current = self.node_start;
        self.nodes[self.node_start].local_goal = 0.0;
        self.nodes[self.node_start].global_goal = Self::heuristic(&self.nodes[self.node_start], &self.nodes[self.node_end]);

        let mut untested_nodes = vec![self.node_start];

        // Guaranted to be the shortest path:
        // while ! untested_nodes.is_empty() {
        // Not guaranted but still valid path (and more performant):
        while ! untested_nodes.is_empty()  && node_current != self.node_end {
            info!("untested: before sort -> {:?}.", untested_nodes.iter()
                  .take(2)
                  .map(|&x| self.nodes[x].global_goal)
                  .collect::<Vec<f32>>());
            untested_nodes.sort_by(|&a, &b|
                if self.nodes[a].global_goal < self.nodes[b].global_goal { Ordering::Less }
                else if self.nodes[a].global_goal > self.nodes[b].global_goal { Ordering::Greater }
                else { Ordering::Equal }
            );
            info!("untested: after sort -> {:?}.", untested_nodes.iter()
                  .take(2)
                  .map(|&x| self.nodes[x].global_goal)
                  .collect::<Vec<f32>>());

            untested_nodes.retain(|&idx| ! self.nodes[idx].visited);
            info!("untested: after retain -> {:?}.", untested_nodes.iter()
                  .take(2)
                  .map(|&x| self.nodes[x].global_goal)
                  .collect::<Vec<f32>>());

            if untested_nodes.is_empty() {
                break;
            }

            node_current = untested_nodes[0];
            self.nodes[node_current].visited = true;
            info!("{}", node_current == self.node_start);

            for &neighbour in &self.neighbours[node_current] {
                if !self.nodes[neighbour].visited && !self.nodes[neighbour].obstacle {
                    untested_nodes.push(neighbour);
                }

                let possibly_lowest_goal = self.nodes[node_current].local_goal + Self::distance(&self.nodes[node_current], &self.nodes[neighbour]);
                info!("{} < {} == {}", possibly_lowest_goal, self.nodes[node_current].local_goal, possibly_lowest_goal < self.nodes[node_current].local_goal);

                if possibly_lowest_goal < self.nodes[neighbour].local_goal {
                    self.parent[neighbour] = node_current;
                    info!("node:: {}, {} => {} ({})", node_current, neighbour, self.parent[neighbour], self.node_end);
                    self.nodes[neighbour].local_goal = possibly_lowest_goal;

                    self.nodes[neighbour].global_goal = self.nodes[neighbour].local_goal + Self::heuristic(&self.nodes[neighbour], &self.nodes[self.node_end]);
                }
            }
        }
    }
}

impl EngineInterface for AStar {
    fn on_user_create(&mut self, _: &mut ScreenBuffer) -> bool {
        for x in 0..self.width {
            for y in 0..self.height {
                self.nodes[(y * self.width + x) as usize].x = x;
                self.nodes[(y * self.width + x) as usize].y = y;
            }
        }

        for x in 0..self.width {
            for y in 0..self.height {
                if y > 0 {
                    self.neighbours[(y * self.width + x) as usize].push(((y - 1) * self.width + (x + 0)) as usize);
                }

                if y < self.height - 1 {
                    self.neighbours[(y * self.width + x) as usize].push(((y + 1) * self.width + (x + 0)) as usize);
                }

                if x > 0 {
                    self.neighbours[(y * self.width + x) as usize].push(((y + 0) * self.width + (x - 1)) as usize);
                }

                if x < self.width - 1 {
                    self.neighbours[(y * self.width + x) as usize].push(((y + 0) * self.width + (x + 1)) as usize);
                }

                // We can also connect the diagonals:
                if y > 0 && x > 0 {
                    self.neighbours[(y * self.width + x) as usize].push(((y - 1) * self.width + (x - 1)) as usize);
                }

                if y < self.height - 1 && x > 0 {
                    self.neighbours[(y * self.width + x) as usize].push(((y + 1) * self.width + (x - 1)) as usize);
                }

                if y > 0 && x < self.width - 1 {
                    self.neighbours[(y * self.width + x) as usize].push(((y - 1) * self.width + (x + 1)) as usize);
                }

                if y < self.height - 1 && x < self.width - 1 {
                    self.neighbours[(y * self.width + x) as usize].push(((y + 1) * self.width + (x + 1)) as usize);
                }
            }
        }

        self.solve_astar();

        true
    }

    fn on_user_update(&mut self, olc: &mut ScreenBuffer, ev: &Events, _elapsed_time: f32) -> bool {
        let scr_width = olc.width();
        let scr_height = olc.height();
        let node_size = 9;
        let node_border = 2;

        let selected_node_x = ev.mouse_pos_x / node_size;
        let selected_node_y = ev.mouse_pos_y / node_size;

        if ev[ MouseButton::Left ].released {
            if selected_node_x >= 0 && selected_node_x < self.width {
                if selected_node_y >= 0 && selected_node_y < self.height {
                    if ev[ Scancode::LShift ].held {
                        self.node_start = (selected_node_y * self.width + selected_node_x) as usize;
                    } else if ev[ Scancode::LCtrl ].held {
                        self.node_end   = (selected_node_y * self.width + selected_node_x) as usize;
                    } else {
                        self.nodes[ (selected_node_y * self.width + selected_node_x) as usize ].obstacle = ! self.nodes[ (selected_node_y * self.width + selected_node_x) as usize ].obstacle;
                    }

                    self.solve_astar();
                }
            }
        }

        olc.fill(
            0, 0,
            scr_width, scr_height,
            ' ' as u16,
            Colour::BgBlack
        );

        // Drawing links between nodes:
        for x in 0..self.width {
            for y in 0..self.height {
                for &idx in &self.neighbours[(y*self.width + x) as usize] {
                    olc.draw_line(
                        (x * node_size + node_size / 2) as u32, (y * node_size + node_size / 2) as u32,
                        (self.nodes[idx].x * node_size + node_size / 2) as u32, (self.nodes[idx].y * node_size + node_size / 2) as u32,
                        PIXEL_SOLID,
                        Colour::FgDarkBlue
                    );
                }
            }
        }

        // Drawing node themselves:
        for x in 0..self.width {
            for y in 0..self.height {
                olc.fill(
                    (x * node_size + node_border) as u32,
                    (y * node_size + node_border) as u32,

                    ((x + 1) * node_size - node_border) as u32,
                    ((y + 1) * node_size - node_border) as u32,

                    PIXEL_HALF,

                    if self.nodes[ (y * self.width + x) as usize ].obstacle     { Colour::FgWhite }
                    else                                                        { Colour::FgBlue  }
                );

                if self.nodes[ (y * self.width + x) as usize ].visited {
                    olc.fill(
                        (x * node_size + node_border) as u32,
                        (y * node_size + node_border) as u32,

                        ((x + 1) * node_size - node_border) as u32,
                        ((y + 1) * node_size - node_border) as u32,

                        PIXEL_SOLID,

                        Colour::FgBlue
                    );
                }
                if (y * self.width + x) as usize == self.node_start    {
                    olc.fill(
                        (x * node_size + node_border) as u32,
                        (y * node_size + node_border) as u32,

                        ((x + 1) * node_size - node_border) as u32,
                        ((y + 1) * node_size - node_border) as u32,

                        PIXEL_SOLID,

                        Colour::FgGreen
                    );
                }
                if (y * self.width + x) as usize == self.node_end      {
                    olc.fill(
                        (x * node_size + node_border) as u32,
                        (y * node_size + node_border) as u32,

                        ((x + 1) * node_size - node_border) as u32,
                        ((y + 1) * node_size - node_border) as u32,

                        PIXEL_SOLID,

                        Colour::FgRed
                    );
                }
            }
        }

        // Drawing the path between start and end nodes:
        info!("Just before line draw");
        if self.node_end != usize::max_value() {
            let mut p_idx = self.node_end;

            info!("Just before while draw ({}, {}) {} != {}", self.node_end, p_idx, self.parent[p_idx], usize::max_value());
            while self.parent[ p_idx ] != usize::max_value() {
                info!("Drawing line");
                olc.draw_line(
                    // Child node:
                    (self.nodes[p_idx].x * node_size + node_size / 2) as u32, (self.nodes[p_idx].y * node_size + node_size / 2) as u32,

                    // Parent node:
                    (self.nodes[self.parent[p_idx]].x * node_size + node_size / 2) as u32, (self.nodes[self.parent[p_idx]].y * node_size + node_size / 2) as u32,

                    PIXEL_SOLID,

                    Colour::FgYellow
                );

                // Setting next node to the parent:
                p_idx = self.parent[p_idx];
            }
        }

        true
    }
}

pub fn main() -> Result<(), String> {
    match simple_logger::init_with_level(Level::Info) {
        Ok(_) => {},
        Err(e) => return Err(format!("Error while initialising the logger: {:?}.", e)),
    };

    let astar = Box::new(AStar::new(16, 16));

    info!("Building the engine.");
    let mut olc = OlcConsoleGameEngineBuilder::new()
        .screen_width(160)
        .screen_height(160)
        .font_width(6)
        .font_height(6)
        .title("A*".to_owned())
        .fps(60.)
        .build(astar)?;

    info!("Starting the main loop.");
    olc.start()
}
