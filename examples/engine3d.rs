extern crate olc_console_game_engine;

use olc_console_game_engine::{EngineInterface, OlcConsoleGameEngineBuilder, ScreenBuffer, Events, PIXEL_SOLID, Colour};

use std::f32;
use std::ops::Mul;

#[derive(Copy,Clone)]
struct Vec3d {
    x: f32,
    y: f32,
    z: f32,
}

#[derive(Copy,Clone)]
struct Matrix4x4 {
    m: [f32; 16],
}

impl Default for Matrix4x4 {
    fn default() -> Self {
        Self {
            m: [0f32; 16],
        }
    }
}

impl Mul<Vec3d> for Matrix4x4 {
    type Output = Vec3d;

    fn mul(self, rhs: Vec3d) -> Self::Output {
        let mut w = rhs.x * self.m[3 * 4 + 0] + rhs.y * self.m[3 * 4 + 1] + rhs.z * self.m[3 * 4 + 2] + self.m[3 * 4 + 3];
        if w == 0f32 {
            w = 1f32;
        }

        Vec3d {
            x: (rhs.x * self.m[0 * 4 + 0] + rhs.y * self.m[0 * 4 + 1] + rhs.z * self.m[0 * 4 + 2] + self.m[0 * 4 + 3]) / w,
            y: (rhs.x * self.m[1 * 4 + 0] + rhs.y * self.m[1 * 4 + 1] + rhs.z * self.m[1 * 4 + 2] + self.m[1 * 4 + 3]) / w,
            z: (rhs.x * self.m[2 * 4 + 0] + rhs.y * self.m[2 * 4 + 1] + rhs.z * self.m[2 * 4 + 2] + self.m[2 * 4 + 3]) / w,
        }
    }
}

struct Triangle {
    p: [Vec3d; 3],
}

struct Mesh {
    tris: Vec<Triangle>
}

struct Engine3D {
    mesh_cube: Mesh,
    proj: Matrix4x4,
}

impl Engine3D {
    fn new() -> Self {
        Engine3D {
            mesh_cube: Mesh {
                tris: Vec::new()
            },
            proj: Default::default(),
        }
    }
}

impl EngineInterface for Engine3D {
    fn on_user_create(&mut self, olc: &mut ScreenBuffer) -> bool {
        // South
        self.mesh_cube.tris.push(
            Triangle { p: [ Vec3d { x: 0., y: 0., z: 0.}, Vec3d { x: 0., y: 1., z: 0.}, Vec3d { x: 1., y: 1., z: 0.}, ] }
        );
        self.mesh_cube.tris.push(
            Triangle { p: [ Vec3d { x: 0., y: 0., z: 0.}, Vec3d { x: 1., y: 1., z: 0.}, Vec3d { x: 1., y: 0., z: 1.}, ] }
        );

        // East
        self.mesh_cube.tris.push(
            Triangle { p: [ Vec3d { x: 1., y: 0., z: 0.}, Vec3d { x: 1., y: 1., z: 0.}, Vec3d { x: 1., y: 1., z: 1.}, ] }
        );
        self.mesh_cube.tris.push(
            Triangle { p: [ Vec3d { x: 1., y: 0., z: 0.}, Vec3d { x: 1., y: 1., z: 1.}, Vec3d { x: 1., y: 0., z: 1.}, ] }
        );

        // North
        self.mesh_cube.tris.push(
            Triangle { p: [ Vec3d { x: 1., y: 0., z: 1.}, Vec3d { x: 1., y: 1., z: 1.}, Vec3d { x: 0., y: 1., z: 1.}, ] }
        );
        self.mesh_cube.tris.push(
            Triangle { p: [ Vec3d { x: 1., y: 0., z: 1.}, Vec3d { x: 0., y: 1., z: 1.}, Vec3d { x: 0., y: 0., z: 1.}, ] }
        );

        // West
        self.mesh_cube.tris.push(
            Triangle { p: [ Vec3d { x: 0., y: 0., z: 1.}, Vec3d { x: 0., y: 1., z: 1.}, Vec3d { x: 0., y: 1., z: 0.}, ] }
        );
        self.mesh_cube.tris.push(
            Triangle { p: [ Vec3d { x: 0., y: 0., z: 1.}, Vec3d { x: 0., y: 1., z: 0.}, Vec3d { x: 0., y: 0., z: 0.}, ] }
        );

        // Top
        self.mesh_cube.tris.push(
            Triangle { p: [ Vec3d { x: 0., y: 1., z: 0.}, Vec3d { x: 0., y: 1., z: 1.}, Vec3d { x: 1., y: 1., z: 1.}, ] }
        );
        self.mesh_cube.tris.push(
            Triangle { p: [ Vec3d { x: 0., y: 1., z: 0.}, Vec3d { x: 1., y: 1., z: 1.}, Vec3d { x: 1., y: 1., z: 0.}, ] }
        );

        // Bottom
        self.mesh_cube.tris.push(
            Triangle { p: [ Vec3d { x: 1., y: 0., z: 1.}, Vec3d { x: 0., y: 0., z: 1.}, Vec3d { x: 0., y: 0., z: 0.}, ] }
        );
        self.mesh_cube.tris.push(
            Triangle { p: [ Vec3d { x: 1., y: 0., z: 1.}, Vec3d { x: 0., y: 0., z: 0.}, Vec3d { x: 1., y: 0., z: 0.}, ] }
        );

        let f_near = 0.1;
        let f_far  = 1000.0;
        let fov    = 90.0;
        let f_aspect_ratio = olc.height() as f32 / olc.width() as f32;
        let f_fov_rad = 1f32 / (fov * 0.5f32).to_radians().tan();

        self.proj.m[0 * 4 + 0] = f_aspect_ratio * f_fov_rad;
        self.proj.m[1 * 4 + 1] = f_fov_rad;
        self.proj.m[2 * 4 + 2] = f_far / (f_far - f_near);
        self.proj.m[2 * 4 + 3] = (-f_far * f_near) / (f_far - f_near);
        self.proj.m[3 * 4 + 2] = 1.0;
        self.proj.m[3 * 4 + 3] = 0.0;
        // self.proj.m[1 * 4 + 1] =

        true
    }

    fn on_user_update(&mut self, olc: &mut ScreenBuffer, _: &Events, elapsed: f32) -> bool {
        let s_w = olc.width();
        let s_h = olc.height();
        let mut rot_z: Matrix4x4 = Default::default();
        let mut rot_x: Matrix4x4 = Default::default();

        olc.fill(0, 0, s_w, s_h, PIXEL_SOLID, Colour::FgBlack);

        rot_z.m[0 * 4 + 0] = elapsed.cos();
        rot_z.m[1 * 4 + 0] = elapsed.sin();
        rot_z.m[0 * 4 + 1] = -elapsed.sin();
        rot_z.m[1 * 4 + 1] = elapsed.cos();
        rot_z.m[2 * 4 + 2] = 1.0;
        rot_z.m[3 * 4 + 3] = 1.0;

        rot_x.m[0 * 4 + 0] = 1.0;
        rot_x.m[1 * 4 + 0] = (elapsed / 2.).cos();
        rot_x.m[0 * 4 + 1] = (elapsed / 2.).sin();
        rot_x.m[1 * 4 + 1] = -(elapsed / 2.).sin();
        rot_x.m[2 * 4 + 2] = (elapsed / 2.).cos();
        rot_x.m[3 * 4 + 3] = 1.0;

        // Draw triangles:
        for tri in &self.mesh_cube.tris {
            let mut tri_trans = Triangle {
                p: [
                    rot_x * (rot_z * tri.p[0]),
                    rot_x * (rot_z * tri.p[1]),
                    rot_x * (rot_z * tri.p[2]),
                ]
            };
            // tri_trans.p[0].z += 3.0;
            // tri_trans.p[1].z += 3.0;
            // tri_trans.p[2].z += 3.0;

            let mut tri_proj = Triangle {
                p: [
                    self.proj * tri_trans.p[0],
                    self.proj * tri_trans.p[1],
                    self.proj * tri_trans.p[2],
                ]
            };

            tri_proj.p[0].x += 1.0; tri_proj.p[0].y += 1.0;
            tri_proj.p[1].x += 1.0; tri_proj.p[1].y += 1.0;
            tri_proj.p[2].x += 1.0; tri_proj.p[2].y += 1.0;

            tri_proj.p[0].x *= 0.5 * s_w as f32;
            tri_proj.p[0].y *= 0.5 * s_h as f32;
            tri_proj.p[1].x *= 0.5 * s_w as f32;
            tri_proj.p[1].y *= 0.5 * s_h as f32;
            tri_proj.p[2].x *= 0.5 * s_w as f32;
            tri_proj.p[2].y *= 0.5 * s_h as f32;

            eprintln!("p0: {}, {}", tri_proj.p[0].x, tri_proj.p[0].y);
            eprintln!("p1: {}, {}", tri_proj.p[1].x, tri_proj.p[1].y);
            eprintln!("p2: {}, {}", tri_proj.p[2].x, tri_proj.p[2].y);

            olc.draw_triangle(
                tri_proj.p[0].x.trunc() as u32, tri_proj.p[0].y.trunc() as u32,
                tri_proj.p[1].x.trunc() as u32, tri_proj.p[1].y.trunc() as u32,
                tri_proj.p[2].x.trunc() as u32, tri_proj.p[2].y.trunc() as u32,
                PIXEL_SOLID,
                Colour::FgWhite
            );
        }

        true
    }
}

fn main() -> Result<(), String> {
    let engine = Box::new(Engine3D::new());

    let mut olc = OlcConsoleGameEngineBuilder::new()
        .screen_width(256)
        .screen_height(240)
        .font_height(4)
        .font_width(4)
        .title("3D engine".to_owned())
        .build(engine)?;

    olc.start()
}
