#[macro_use]
extern crate log;
extern crate simple_logger;
extern crate olc_console_game_engine;

use olc_console_game_engine::{EngineInterface, OlcConsoleGameEngineBuilder, ScreenBuffer, Events, Colour, PIXEL_SOLID};

#[derive(Debug)]
struct Circle {
    x: u32,
    y: u32,
    r: i32,
}

impl EngineInterface for Circle {
    fn on_user_create(&mut self, _: &mut ScreenBuffer) -> bool {
        true
    }

    fn on_user_update(&mut self, olc: &mut ScreenBuffer, _: &Events, elapsed_time: f32) -> bool {
        olc.fill(10, 10, self.x, self.y, PIXEL_SOLID, Colour::BgYellow);
        olc.draw_circle(self.x, self.y, self.r, PIXEL_SOLID, Colour::BgBlue);
        olc.draw_string_alpha(10, 12, &format!("FPS: {}", elapsed_time), Colour::FgBlack);
        olc.draw_string_alpha(10, 20, "Test d'affichage de chaîne.", Colour::FgRed);

        true
    }
}

pub fn main() -> Result<(), String> {
    match simple_logger::init() {
        Ok(_) => {},
        Err(e) => return Err(format!("Error while initialising the logger: {:?}.", e)),
    };

    let circle = Box::new(Circle {
        x: 20,
        y: 15,
        r: 10,
    });

    debug!("Circle is {:?}", circle);

    info!("Building the engine.");
    let mut olc = OlcConsoleGameEngineBuilder::new()
        .screen_width(80)
        .screen_height(30)
        .font_width(16)
        .font_height(16)
        .title("3D engine".to_owned())
        .build(circle)?;

    info!("Starting the main loop.");
    olc.start()
}
