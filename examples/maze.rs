#[macro_use]
extern crate log;
extern crate simple_logger;
extern crate rand;
extern crate olc_console_game_engine;

use log::Level;
use rand::random;

use olc_console_game_engine::{EngineInterface, OlcConsoleGameEngineBuilder, ScreenBuffer, Events, Colour, PIXEL_SOLID};

use std::time::Duration;

static CELL_PATH_N: u32 = 0x01;
static CELL_PATH_E: u32 = 0x02;
static CELL_PATH_S: u32 = 0x04;
static CELL_PATH_W: u32 = 0x08;
static CELL_VISITED: u32 = 0x10;

#[derive(Debug)]
struct Maze {
    width: usize,
    height: usize,
    maze: Vec<u32>,

    stack: Vec<(i32, i32)>,

    visited: u32,

    path_width: u32,
}

impl Maze {
    pub fn new(w: usize, h: usize) -> Self {
        Self {
            width: w,
            height: h,

            maze: vec![0u32; w * h],

            stack: Vec::new(),

            visited: 0,
            path_width: 3,
        }
    }

    #[inline]
    fn offset(&self, x: i32, y: i32) -> usize {
        let top = self.stack[self.stack.len() - 1];
        (top.1 + y) as usize * self.width + (top.0 + x) as usize
    }
}

impl EngineInterface for Maze {
    fn on_user_create(&mut self, _: &mut ScreenBuffer) -> bool {
        let x = random::<usize>() % self.width;
        let y = random::<usize>() % self.height;
        self.stack.push((x as i32, y as i32));
        self.maze[y * self.width + x] = CELL_VISITED;

        self.visited = 1;

        true
    }

    fn on_user_update(&mut self, olc: &mut ScreenBuffer, _: &Events, _elapsed_time: f32) -> bool {
        std::thread::sleep(Duration::from_millis(10));

        let top = self.stack[self.stack.len() - 1];
        let width = self.width;
        let offset = |x, y| (top.1 + y) as usize * width + (top.0 + x) as usize;

        if (self.visited as usize) < (self.width * self.height) {
            let mut neighbours = Vec::new();

            // North
            if top.1 > 0 && (self.maze[self.offset(0, -1)] & CELL_VISITED) == 0 {
                neighbours.push(0);
            }

            // East
            if (top.0 as usize) < (self.width - 1) && (self.maze[self.offset(1, 0)] & CELL_VISITED) == 0 {
                neighbours.push(1);
            }

            // South
            if (top.1 as usize) < (self.height - 1) && (self.maze[self.offset(0, 1)] & CELL_VISITED) == 0 {
                neighbours.push(2);
            }

            // West
            if top.0 > 0 && (self.maze[self.offset(-1, 0)] & CELL_VISITED) == 0 {
                neighbours.push(3);
            }

            if ! neighbours.is_empty() {
                let next_cell_dir = neighbours[random::<usize>() % neighbours.len()];

                match next_cell_dir {
                    0 => {
                        self.maze[offset(0, -1)] |= CELL_VISITED | CELL_PATH_S;
                        self.maze[offset(0,  0)] |= CELL_PATH_N;
                        self.stack.push((top.0 + 0, top.1 - 1));
                    },

                    1 => {
                        self.maze[offset(1,  0)] |= CELL_VISITED | CELL_PATH_W;
                        self.maze[offset(0,  0)] |= CELL_PATH_E;
                        self.stack.push((top.0 + 1, top.1 + 0));
                    },

                    2 => {
                        self.maze[offset(0,  1)] |= CELL_VISITED | CELL_PATH_N;
                        self.maze[offset(0,  0)] |= CELL_PATH_S;
                        self.stack.push((top.0 + 0, top.1 + 1));
                    },

                    3 => {
                        self.maze[offset(-1, 0)] |= CELL_VISITED | CELL_PATH_E;
                        self.maze[offset( 0, 0)] |= CELL_PATH_W;
                        self.stack.push((top.0 - 1, top.1 + 0));
                    },

                    _ => unreachable!(),
                }

                self.visited += 1;
            } else {
                self.stack.pop();
            }
        }

        let s_width = olc.width();
        let s_height = olc.height();
        olc.fill(0, 0, s_width, s_height, ' ' as u16, Colour::FgWhite);

        for x in 0..self.width {
            for y in 0..self.height {
                for py in 0..self.path_width {
                    for px in 0..self.path_width {
                        if self.maze[ y * self.width + x ] & CELL_VISITED  != 0 {
                            olc[( (x as u32) * (self.path_width + 1) + px, (y as u32) * (self.path_width + 1) + py )].glyph = PIXEL_SOLID;
                            olc[( (x as u32) * (self.path_width + 1) + px, (y as u32) * (self.path_width + 1) + py )].colour = Colour::FgWhite;
                        } else {
                            olc[( (x as u32) * (self.path_width + 1) + px, (y as u32) * (self.path_width + 1) + py )].glyph = PIXEL_SOLID;
                            olc[( (x as u32) * (self.path_width + 1) + px, (y as u32) * (self.path_width + 1) + py )].colour = Colour::FgBlue;
                        }
                    }
                }

                for p in 0..self.path_width {
                    if self.maze[y * self.width + x] & CELL_PATH_S != 0 {
                        olc[((x as u32) * (self.path_width + 1) + p, (y as u32) * (self.path_width + 1) + self.path_width)].glyph = PIXEL_SOLID;
                        olc[((x as u32) * (self.path_width + 1) + p, (y as u32) * (self.path_width + 1) + self.path_width)].colour = Colour::FgWhite;
                    }

                    if self.maze[y * self.width + x] & CELL_PATH_E != 0 {
                        olc[((x as u32) * (self.path_width + 1) + self.path_width, (y as u32) * (self.path_width + 1) + p)].glyph = PIXEL_SOLID;
                        olc[((x as u32) * (self.path_width + 1) + self.path_width, (y as u32) * (self.path_width + 1) + p)].colour = Colour::FgWhite;
                    }
                }
            }
        }

        let current = self.stack[self.stack.len() - 1];
        for py in 0..self.path_width {
            for px in 0..self.path_width {
                olc[((current.0 as u32) * (self.path_width + 1) + px, (current.1 as u32) * (self.path_width + 1) + py)].glyph = 0x2588;
                olc[((current.0 as u32) * (self.path_width + 1) + px, (current.1 as u32) * (self.path_width + 1) + py)].colour = Colour::FgGreen;
            }
        }

        true
    }
}

pub fn main() -> Result<(), String> {
    match simple_logger::init_with_level(Level::Error) {
        Ok(_) => {},
        Err(e) => return Err(format!("Error while initialising the logger: {:?}.", e)),
    };

    let maze = Box::new(Maze::new(40, 25));

    info!("Building the engine.");
    let mut olc = OlcConsoleGameEngineBuilder::new()
        .screen_width(160)
        .screen_height(100)
        .font_width(8)
        .font_height(8)
        .title("Maze".to_owned())
        .build(maze)?;

    info!("Starting the main loop.");
    olc.start()
}
